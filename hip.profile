<?php

/**
 * Implementation of hook_profile_details().
 */
function hip_profile_details() {
  return array(
    'name' => 'Happiness Installation Profile',
    'description' => 'Hip (Happiness installation profile) for simple corporate website with some generated content.',
  );
}

/**
 * Implementation of hook_profile_task_list().
 */
function hip_profile_task_list() {
  global $conf;
  $conf['site_name'] = 'Hip installation guide';
  $conf['site_footer'] = 'Happiness Installation Profile by <a href="http://happiness.se">Happiness</a>';
  return array();
}

/**
 * Implementation of hook_profile_modules().
 */
function hip_profile_modules() {
  $core = array(
    'dblog',
    'help',
    'locale',
    'menu',
    'path',
    'taxonomy',
    'update',
  );
  $contrib = array(
    'admin_menu',
    'backup_migrate',
    'content',
    'ctools',
    'ctm',
    'date_api',
    'date',
    'date_popup',
    'date_timezone',
    'dhtml_menu',
    'ds',
    'ds_ui',
    'emfield',
    'emimage',
    'emthumb',
    'emvideo',
    'features',
    'feed_path_publisher',
    'filefield',
    'globalredirect',
    'imageapi',
    'imageapi_gd',
    'imagecache',
    'imagecache_ui',
    'imagefield',
    'jquery_ui',
    'jquery_update',
    'link',
    'menu_block',
    'menutrails',
    'modalframe',
    'nd',
    'nd_cck',
    'nodesinblock',
    'nodereference',
    'nodewords',
    'nodewords_basic',
    'noderelationships',
    'optionwidgets',
    'pathologic',
    'linkit',
    'page_title',
    'pathauto',
    'prepopulate',
    'reference_preview',
    'rules',
    'cache_actions',
    'spamspan',
    'text',
    'token',
    'transliteration',
    'scheduler',
    'semanticviews',
    'strongarm',
    'vertical_tabs',
    'views',
    'views_slideshow',
    'views_slideshow_singleframe',
    'views_ui',
    'viewfield',
    'webform',
    'wysiwyg',
    'xmlsitemap',
  );
  $hip = array(
    'hip_core',
    'hip_admin',
    'hip_defaults',
    'hip_attachment',
    'hip_image',
    'hip_form',
    'hip_list',
    'hip_news',
    'hip_page',
    'hip_promo',
    'hip_promo_list',
    'hip_video',
  );

  return array_merge($core, $contrib, $hip);
}

/**
 * Helper function for fetching the order of demo content files.
 */
function hip_profile_content_types() {
  return array(
    'image',
    'promo',
    'promo_list',
    'list',
    'page',
    'video',
    'news',
  );
}

/**
 * Implementation of hook_profile_tasks().
 */
function hip_profile_tasks(&$task, $url) {
  $batch = array();

  if ($task == 'profile') {
    $batch = hip_profile_batch_get();
  }
  return hip_profile_batch_process($task, $url, $batch);
}

/**
 * Returns the default batch.
 */
function hip_profile_batch_get() {
  $batch = array();
  $batch['title'] = st('Configuring !drupal', array('!drupal' => drupal_install_profile_name()));
  $batch['error_message'] = st('There was an error configuring !drupal.', array('!drupal' => drupal_install_profile_name()));
  $batch['operations'][] = array('hip_profile_operation_basic', array());
  $batch['operations'][] = array('hip_profile_operation_term', array());
  foreach (hip_profile_content_types() as $type) {
    $batch['operations'][] = array('hip_profile_operation_content', array($type));
  }
  $batch['finished'] = 'hip_profile_operation_finished';
  return $batch;
}

/**
 * Processes the batch.
 */
function hip_profile_batch_process($task, $url, $batch = array()) {
  if ($task == 'profile') {
    variable_set('install_task', 'hip-batch');
    batch_set($batch);
    batch_process($url, $url);
    // Jut for CLI installs.
    return;
  }
  // We are running a batch task for this profile so do nothing and return page.
  if ($task == 'hip-batch') {
    include_once 'includes/batch.inc';
    $output = _batch_page();
  }
  return $output;
}

/**
 * Install operation for basic configuration.
 */
function hip_profile_operation_basic(&$context) {
  $context['message'] = t('Setting up basic configuration.');

  drupal_flush_all_caches();

  // Set the default theme.
  variable_set('theme_default', 'hipster');
  
  // Set default front page.
  variable_set('site_frontpage', 'node/11');
  
  // Delete the default webform content type.
  node_type_delete('webform');

  // Remove all blocks.
  db_query("TRUNCATE {blocks}");
  
  // Assign default blocks.
  $query = "INSERT INTO {blocks} VALUES (NULL, '%s', '%s', 'hipster', 1, 0, '%s', 0, 0, 0, '', '%s', %d)";
  db_query($query, 'hip_core', 'branding', 'header', '<none>', -1);
  db_query($query, 'hip_core', 'navigation', 'header', '<none>', 0);
  db_query($query, 'nodesinblock', '0', 'sidebar_second', '<none>', 4);
  db_query($query, 'nodesinblock', '1', 'sidebar_first', '<none>', 4);
  db_query($query, 'nodesinblock', '2', 'promotion', '<none>', 4);
  db_query($query, 'nodesinblock', '3', 'footer', '<none>', 4);
  db_query($query, 'views', 'slideshow-block_1', 'slideshow', '<none>', 0);
  db_query($query, 'menu_block', '1', 'sidebar_second', '', -1);
  db_query($query, 'views', 'video-block_1', 'slideshow', '<none>', 1);
  db_query($query, 'views', 'attachments-block_1', 'content', '<none>', 0);
  db_query($query, 'views', 'forms-block_1', 'content', '<none>', 1);
  
  // Assign default nodes in block.
  $query = "INSERT INTO {nodesinblock} VALUES (%d, %d, 0, '*', 'promo')";
  db_query($query, 6, 0);
  db_query($query, 7, 0);
  db_query($query, 8, 3);
  db_query($query, 9, 0);
  
  // Node relationships.
  module_load_include('inc', 'noderelationships', 'noderelationships');
  noderelationships_settings_save(
    'hip_page',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_images_ref' => 'noderelationships_noderef:page_grid',
          'field_elements_ref' => 'noderelationships_noderef:page_grid',
        ),
        'view_in_new_window' => array(
          'field_images_ref' => 'field_images_ref',
        ),
        'create_and_reference' => array(
          'field_images_ref' => 'field_images_ref',
          'field_elements_ref' => 'field_elements_ref',
        ),
        'edit_reference' => array(
          'field_images_ref' => 'field_images_ref',
          'field_elements_ref' => 'field_elements_ref',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'hip_news',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_images_ref' => 'noderelationships_noderef:page_grid',
          'field_elements_ref' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_images_ref' => 'field_images_ref',
          'field_elements_ref' => 'field_elements_ref',
        ),
        'edit_reference' => array(
          'field_images_ref' => 'field_images_ref',
          'field_elements_ref' => 'field_elements_ref',
        ),
      ),
    )
  );
  noderelationships_settings_save(
    'hip_promo',
    array(
      'noderef' => array(
        'search_and_reference_view' => array(
          'field_promo_image_ref' => 'noderelationships_noderef:page_grid',
        ),
        'create_and_reference' => array(
          'field_promo_image_ref' => 'field_promo_image_ref',
        ),
        'edit_reference' => array(
          'field_promo_image_ref' => 'field_promo_image_ref',
        ),
      ),
    )
  );
  
  // Configure input format settings.
  $query = 'UPDATE {filter_formats} SET name = "%s", roles = "%s" WHERE format = %d';
  db_query($query, t('Plain text'), ',1,2,3,4,', 1);
  db_query($query, t('HTML editor'), ',3,4,', 2);
  db_query('INSERT INTO {filter_formats} VALUES (NULL, "Simple HTML editor", ",3,4,", 1)');
  db_query($query, t('Simple HTML editor'), ',3,4,', 3);

  // Configure input format filters.
  db_query('TRUNCATE {filters}');
  $query = 'INSERT INTO {filters} (format, module, delta, weight) VALUES (%d, "%s", %d, %d)';
  db_query($query, 1, 'filter', 0, -8);
  db_query($query, 1, 'filter', 1, -10);
  db_query($query, 1, 'filter', 2, -9);
  db_query($query, 1, 'filter', 3, -7);
  db_query($query, 2, 'filter', 1, -7);
  db_query($query, 2, 'filter', 2, -8);
  db_query($query, 2, 'filter', 3, -9);
  db_query($query, 2, 'spamspan', 0, 10);
  db_query($query, 2, 'pathologic', 0, 10);
  db_query($query, 3, 'filter', 1, -7);
  db_query($query, 3, 'filter', 2, -8);
  db_query($query, 3, 'filter', 3, -9);
  db_query($query, 3, 'spamspan', 0, 10);
  db_query($query, 3, 'pathologic', 0, 10);
  
  // Configure WYSIWYG.
  $settings = array (
    'default' => 1,
    'user_choose' => 0,
    'show_toggle' => 1,
    'theme' => 'advanced',
    'language' => 'sv',
    'buttons' => array (
      'default' => array (
        'Linkit' => 1,
      ),
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'left',
    'path_loc' => 'bottom',
    'resizing' => 1,
    'verify_html' => 0,
    'preformatted' => 0,
    'convert_fonts_to_spans' => 0,
    'remove_linebreaks' => 0,
    'apply_source_formatting' => 0,
    'paste_auto_cleanup_on_paste' => 0,
    'block_formats' => 'p,h2,h3',
    'css_setting' => 'none',
    'css_path' => '',
    'css_classes' => '',
  );
  $query = 'INSERT INTO {wysiwyg} (format, editor, settings) VALUES (%d, "%s", "%s")';
  db_query($query, 1, '', '');
  db_query($query, 2, 'ckeditor', serialize($settings));
  db_query($query, 3, 'ckeditor', serialize($settings));
  
  // Feed path publisher.
  db_query('INSERT INTO {feed_path_publisher} VALUES (NULL, "Nyheter", "", "rss.xml", -10)');
}

/**
 * Install operation for setting up demo content.
 */
function hip_profile_operation_content($type, &$context) {
  $context['message'] = t('Setting up demo content.');
  $user = user_load(array('uid' => 1));
  $nodes = array();

  require_once dirname(__FILE__) . '/content/' . $type . '.inc';

  foreach ($nodes as $node) {
    $node = (object)$node;
    node_save($node);
  }
}

/**
 * Install operation for setting up terms.
 */
function hip_profile_operation_term(&$context) {
  $context['message'] = t('Setting up taxonomy terms.');
  $terms = array();

  require_once dirname(__FILE__) . '/content/terms.inc';

  foreach ($terms as $term) {
    taxonomy_save_term($term);
  }
}

/**
 * Last install operation.
 */
function hip_profile_operation_finished($success, $results, $operations) { 
  // Revert key components that are overridden by others on install.
  // Note that this comes after all other processes have run, as some cache
  // clears/rebuilds actually set variables or other settings that would count
  // as overrides.
  $revert = array(
    'hip_defaults' => array('variable'),
  );
  features_revert($revert);
  
  // Make default theme active.
  db_query('UPDATE {system} SET status = 1 WHERE name = "hipster"');

  // Rebuild node access now when we've created some content.
  node_access_rebuild(); 

  // Let's finnish the install.
  variable_set('install_task', 'profile-finished');
}