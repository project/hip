; Core

core = "6.19"
api = "2"
projects[] = "drupal"

; Modules

projects[admin_menu][version] = "1.6"
projects[backup_migrate][version] = "2.4"
projects[cache_actions][version] = "1.0"
projects[cck][version] = "2.8"
projects[ctools][version] = "1.8"
projects[ctm][version] = "1.0"
projects[date][version] = "2.6"
projects[dhtml_menu][version] = "3.5"
projects[ds][version] = "1.3"
projects[emfield][version] = "1.25"
projects[features][version] = "1.0"
projects[feed_path_publisher][version] = "1.2"
projects[filefield][version] = "3.7"
projects[globalredirect][version] = "1.2"
projects[google_analytics][version] = "3.0"
projects[imageapi][version] = "1.9"
projects[imagecache][version] = "2.0-beta10"
projects[imagefield][version] = "3.7"
projects[jquery_ui][version] = "1.4"
projects[jquery_update][version] = "2.0-alpha1"
projects[link][version] = "2.9"
projects[linkit][version] = "1.7"
projects[menu_block][version] = "2.3"
projects[menutrails][version] = "1.1"
projects[modalframe][version] = "1.7"
projects[nd][version] = "2.3"
projects[nd_contrib][version] = "2.3"
projects[nodesinblock][version] = "1.6"
projects[noderelationships][version] = "1.6"
projects[nodewords][version] = "1.11"
projects[page_title][version] = "2.3"
projects[pathologic][version] = "3.3"
projects[pathauto][version] = "1.4"
projects[prepopulate][version] = "2.1"
projects[reference_preview][version] = "1.x-dev"
projects[rules][version] = "1.3"
projects[scheduler][version] = "1.7"
projects[semanticviews][version] = "1.1"
projects[spamspan][version] = "1.4"
projects[strongarm][version] = "2.0"
projects[token][version] = "1.14"
projects[transliteration][version] = "3.0"
projects[vertical_tabs][version] = "1.0-rc1"
projects[viewfield][version] = "1.0"
projects[views][version] = "2.11"
projects[views_slideshow][version] = "2.3"
projects[webform][version] = "3.4"
projects[wysiwyg][version] = "2.1"
projects[xmlsitemap][version] = "2.0-beta1"

projects[hip_core][type] = "module"
projects[hip_core][download][type] = "git"
projects[hip_core][download][url] = "git://github.com/happiness/hip_core.git"

; Themes

projects[seven][version] = "1.0-beta11"

projects[hipster][type] = "theme"
projects[hipster][download][type] = "git"
projects[hipster][download][url] = "git://github.com/happiness/hipster.git"

; Patches

; http://drupal.org/node/362065
projects[menutrails][patch][] = "http://drupal.org/files/issues/menutrails-362065-2.patch"

; http://drupal.org/node/877386
projects[seven][patch][] = "http://drupal.org/files/issues/seven-877386-6-fix-fieldsets.patch"

; http://drupal.org/node/878560
projects[seven][patch][] = "http://drupal.org/files/issues/878560-add-more-margin-1.patch"

; http://drupal.org/node/874776
projects[seven][patch][] = "http://drupal.org/files/issues/874776-textfield-width-3.patch"

; http://drupal.org/node/931024
projects[date][patch][] = "http://drupal.org/files/issues/date-931024.patch"

; http://drupal.org/node/881270
projects[pathauto][patch][] = "http://drupal.org/files/issues/pathauto-881270-14.patch"

; Set fixed width on modal dialog
projects[noderelationships][patch][] = "http://happiness.se/noderelationships-modal-width.patch"

; http://drupal.org/node/954192
projects[nodesinblock][patch][] = "http://drupal.org/files/issues/nodesinblock-954192.patch"

; Libraries

libraries[jquery_ui][download][type] = "get"
libraries[jquery_ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery-ui-1.7.3.zip"
libraries[jquery_ui][destination] = "modules/jquery_ui"
libraries[jquery_ui][directory_name] = "jquery.ui"

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.4/ckeditor_3.4.tar.gz"
libraries[ckeditor][destination] = "libraries"
