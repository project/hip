<?php
$base = array(
  'type' => 'hip_video',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 13
  $base + array(
    'title' => 'Video 1',
    'field_video_url' => array(
      '0' => array(
        'embed' => 'http://vimeo.com/10283321',
        'value' => '10283321',
        'provider' => 'vimeo',
      ),
    ),
  ),
);
