<?php
$base = array(
  'type' => 'hip_page',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 11
  $base + array(
    'title' => 'Happiness är en webbyrå',
    'field_page_title' => array(
      '0' => array(
        'value' => 'Happiness är en webbyrå',
      ),
    ),
    'field_page_sub_header' => array(
      '0' => array(
        'value' => 'Vi gör webbplatser',
      ),
    ),
    'field_page_lead' => array(
      '0' => array(
        'value' => 'Precis! Vi på Happiness producerar webbplatser och kampanjer för företag och organisationer som SkandiaMäklarna, CityAkuten, Grand Hôtel Stockholm, Wallenbergstiftelserna, Skolverket, Stockholms Läns Landsting och många fler.',
        'format' => '3',
      ),
    ),
    'field_page_body' => array(
      '0' => array(
        'value' => '<p>Vår kompetens finns i tre områden:</p><ul><li><strong>Teknik</strong> - Programmering och&nbsp;systemutveckling&nbsp;</li><li><strong>Kommunikation</strong> - Gränssnittsdesign,&nbsp;interaktionsdesign och&nbsp;användarupplevelse</li><li><strong>Strategi</strong> - Webbstrategi, koncept och&nbsp;informationsarkitektur</li></ul>',
        'format' => '2',
      ),
    ),
    'field_images_ref' => array(
      '0' => array(
        'nid' => 3,
      ),
      '1' => array(
        'nid' => 5,
      ),
    ),
    'field_elements_ref' => array(
      '0' => array(
        'nid' => NULL,
      ),
    ),
    'menu' => array(
      'link_title' => 'Hem',
      'menu_name' => 'primary-links',
      'plid' => 0,
      'weight' => 0,
    ),
  ),
  // Nid 12
  $base + array(
    'title' => 'Nyheter',
    'field_page_title' => array(
      '0' => array(
        'value' => 'Nyheter',
      ),
    ),
    'field_page_sub_header' => array(
      '0' => array(
        'value' => 'In sollicitudin mattis leo in mollis. Maecenas vehicula sed.',
      ),
    ),
    'field_page_lead' => array(
      '0' => array(
        'value' => 'Aliquam erat volutpat. Sed suscipit purus at metus ullamcorper scelerisque. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris nec amet.',
        'format' => '3',
      ),
    ),
    'field_page_body' => array(
      '0' => array(
        'value' => '',
        'format' => '2',
      ),
    ),
    'field_images_ref' => array(
      '0' => array(
        'nid' => NULL,
      ),
    ),
    'field_elements_ref' => array(
      '0' => array(
        'nid' => 10,
      ),
    ),
    'menu' => array(
      'link_title' => 'Nyheter',
      'menu_name' => 'primary-links',
      'plid' => 0,
      'weight' => 1,
    ),
  ),
);
