<?php
$base = array(
  'type' => 'hip_news',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 14
  $base + array(
    'title' => 'Praesent eget volutpat tortorå',
    'field_news_date' => array(
      '0' => array(
        'value' => '2011-01-01T00:00:00',
        'timezone' => 'Europe/Stockholm',
        'timezone_db' => 'Europe/Stockholm',
        'date_type' => 'date',
      ),
    ),
    'field_news_title' => array(
      '0' => array(
        'value' => 'Praesent eget volutpat tortorå',
      ),
    ),
    'field_news_lead' => array(
      '0' => array(
        'value' => 'In mauris justo, tempus vitae scelerisque nec, adipiscing mattis mauris. Cras ut risus adipiscing dui pellentesque auctor. Donec enim sapien, placerat sed tincidunt sed massa nunc. ',
        'format' => '3',
      ),
    ),
    'field_next_body' => array(
      '0' => array(
        'value' => '<p>Sed dictum dui ut urna interdum tincidunt? Nam ac augue in nibh tincidunt volutpat in et lectus. Ut ullamcorper volutpat dolor, ac mollis orci ultrices in. Etiam at mi massa. Vestibulum sollicitudin fringilla ipsum. Aenean vel nisi est. Sed ut urna metus. Donec sodales nisl vitae elit ultrices aliquet.</p><p>Vivamus placerat nisi sed mauris accumsan ornare? Vestibulum hendrerit velit faucibus nisi scelerisque quis pretium tortor dictum. Suspendisse potenti. Quisque tincidunt magna sit amet massa viverra non aliquet velit dignissim. Duis luctus, quam et ultricies imperdiet, est nunc accumsan lorem, euismod auctor magna orci vitae quam. In hac habitasse platea dictumst.</p>',
        'format' => '2',
      ),
    ),
    'taxonomy' => array(
      1 => array(
        1 => 1,
      ),
    ),
  ),
  // Nid 15
  $base + array(
    'title' => 'Integer tincidunt velit a neque elementum',
    'field_news_date' => array(
      '0' => array(
        'value' => '2011-02-01T00:00:00',
        'timezone' => 'Europe/Stockholm',
        'timezone_db' => 'Europe/Stockholm',
        'date_type' => 'date',
      ),
    ),
    'field_news_title' => array(
      '0' => array(
        'value' => 'Integer tincidunt velit a neque elementum',
      ),
    ),
    'field_news_lead' => array(
      '0' => array(
        'value' => 'In mauris justo, tempus vitae scelerisque nec, adipiscing mattis mauris. Cras ut risus adipiscing dui pellentesque auctor. Donec enim sapien, placerat sed tincidunt sed massa nunc. ',
        'format' => '3',
      ),
    ),
    'field_next_body' => array(
      '0' => array(
        'value' => '<p>Sed dictum dui ut urna interdum tincidunt? Nam ac augue in nibh tincidunt volutpat in et lectus. Ut ullamcorper volutpat dolor, ac mollis orci ultrices in. Etiam at mi massa. Vestibulum sollicitudin fringilla ipsum. Aenean vel nisi est. Sed ut urna metus. Donec sodales nisl vitae elit ultrices aliquet.</p><p>Vivamus placerat nisi sed mauris accumsan ornare? Vestibulum hendrerit velit faucibus nisi scelerisque quis pretium tortor dictum. Suspendisse potenti. Quisque tincidunt magna sit amet massa viverra non aliquet velit dignissim. Duis luctus, quam et ultricies imperdiet, est nunc accumsan lorem, euismod auctor magna orci vitae quam. In hac habitasse platea dictumst.</p>',
        'format' => '2',
      ),
    ),
    'taxonomy' => array(
      1 => array(
        1 => 1,
      ),
    ),
  ),
);
