<?php
$base = array(
  'type' => 'hip_image',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 1
  $base + array(
    'title' => 'Bild 1',
    'field_image_caption' => array(
      '0' => array(
        'value' => 'Maecenas sed enim a ipsum sodales molestie. Etiam cras amet.',
      ),
    ),
    'field_image_credit' => array(
      '0' => array(
        'value' => 'Happiness',
      ),
    ),
    'field_image_file' => array(
      '0' => NULL,
    ),
    'field_image_url' => array(
      '0' => array(
        'embed' => 'http://i959.photobucket.com/albums/ae77/petertornstrand/hip/happiness1.jpg',
        'value' => 'hip/happiness1.jpg',
        'provider' => 'photobucket',
      ),
    ),
  ),
  // Nid 2
  $base + array(
    'title' => 'Bild 2',
    'field_image_caption' => array(
      '0' => array(
        'value' => 'Nulla pellentesque magna at turpis dignissim aliquam nullam.',
      ),
    ),
    'field_image_credit' => array(
      '0' => array(
        'value' => 'Happiness',
      ),
    ),
    'field_image_file' => array(
      '0' => NULL,
    ),
    'field_image_url' => array(
      '0' => array(
        'embed' => 'http://i959.photobucket.com/albums/ae77/petertornstrand/hip/happiness2.png',
        'value' => 'hip/happiness2.png',
        'provider' => 'photobucket',
      ),
    ),
  ),
  // Nid 3
  $base + array(
    'title' => 'Bild 3',
    'field_image_caption' => array(
      '0' => array(
        'value' => 'Aliquam id dolor eros, sit amet gravida lacus. Sed volutpat.',
      ),
    ),
    'field_image_credit' => array(
      '0' => array(
        'value' => 'Happiness',
      ),
    ),
    'field_image_file' => array(
      '0' => NULL,
    ),
    'field_image_url' => array(
      '0' => array(
        'embed' => 'http://i959.photobucket.com/albums/ae77/petertornstrand/hip/happiness3.jpg',
        'value' => 'hip/happiness3.jpg',
        'provider' => 'photobucket',
      ),
    ),
  ),
  // Nid 4
  $base + array(
    'title' => 'Bild 4',
    'field_image_caption' => array(
      '0' => array(
        'value' => 'Vestibulum adipiscing erat in odio gravida sit amet posuere.',
      ),
    ),
    'field_image_credit' => array(
      '0' => array(
        'value' => 'Happiness',
      ),
    ),
    'field_image_file' => array(
      '0' => NULL,
    ),
    'field_image_url' => array(
      '0' => array(
        'embed' => 'http://i959.photobucket.com/albums/ae77/petertornstrand/hip/happiness4.jpg',
        'value' => 'hip/happiness4.jpg',
        'provider' => 'photobucket',
      ),
    ),
  ),
  // Nid 5
  $base + array(
    'title' => 'Bild 5',
    'field_image_caption' => array(
      '0' => array(
        'value' => 'Phasellus nunc sem, ultricies ut viverra vel nullam sodales.',
      ),
    ),
    'field_image_credit' => array(
      '0' => array(
        'value' => 'Happiness',
      ),
    ),
    'field_image_file' => array(
      '0' => NULL,
    ),
    'field_image_url' => array(
      '0' => array(
        'embed' => 'http://i959.photobucket.com/albums/ae77/petertornstrand/hip/happiness6.jpg',
        'value' => 'hip/happiness6.jpg',
        'provider' => 'photobucket',
      ),
    ),
  ),
);
