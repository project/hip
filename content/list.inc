<?php
$base = array(
  'type' => 'hip_list',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 10
  $base + array(
    'title' => 'Nyhetslistning',
    'field_list_view' => array(
      '0' => array(
        'vname' => 'news|default',
        'vargs' => '',
      ),
    ),
  ),
);
