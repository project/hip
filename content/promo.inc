<?php
$base = array(
  'type' => 'hip_promo',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 6
  $base + array(
    'title' => 'Puff 1',
    'field_promo_image_ref' => array(
      '0' => array(
        'nid' => 1,
      ),
    ),
    'field_promo_link' => array(
      '0' => array(
        'url' => 'http://happiness.se/',
        'title' => 'Happiness',
      ),
    ),
    'field_promo_body' => array(
      '0' => array(
        'value' => '<p>Sed pharetra commodo dui, ac aliquet nisl vehicula non.</p>',
        'format' => '2',
      ),
    ),
  ),
  // Nid 7
  $base + array(
    'title' => 'Puff 2',
    'field_promo_image_ref' => array(
      '0' => array(
        'nid' => 2,
      ),
    ),
    'field_promo_link' => array(
      '0' => array(
        'url' => 'http://happiness.se/',
        'title' => 'Happiness',
      ),
    ),
    'field_promo_body' => array(
      '0' => array(
        'value' => '<p>Duis nisi ipsum, pulvinar feugiat dignissim quis, pharetra ac orci!</p>',
        'format' => '2',
      ),
    ),
  ),
  // Nid 8
  $base + array(
    'title' => 'Sidfot',
    'field_promo_image_ref' => array(
      '0' => array(
        'nid' => NULL,
      ),
    ),
    'field_promo_link' => array(
      '0' => array(
        'url' => NULL,
        'title' => NULL,
      ),
    ),
    'field_promo_body' => array(
      '0' => array(
        'value' => '<p>Copyright &copy; 2010, Global Happiness Sweden AB - Tel: 08-458 08 90 - Fax: 08-457 08 93 - E-post: info@happiness.se</p>',
        'format' => '2',
      ),
    ),
  ),
);
