<?php
$base = array(
  'type' => 'hip_promo_list',
  'language' => '',
  'uid' => $user->uid,
  'status' => 1,
  'comment' => 0,
  'promote' => 0,
  'moderate' => 0,
  'sticky' => 0,
  'tnid' => 0,
  'translate' => 0,
  'revision_uid' => $user->uid,
  'format' => 0,
  'name' => $user->name,
);

$nodes = array(
  // Nid 9
  $base + array(
    'title' => 'Puff 3',
    'field_hip_promo_list_view' => array(
      '0' => array(
        'vname' => 'news_promo|default',
        'vargs' => '',
      ),
    ),
    'field_hip_promo_list_body' => array(
      '0' => array(
        'value' => '<h2>Nyheter</h2><p>Sed pharetra commodo dui, ac aliquet nisl vehicula non.</p>',
        'format' => '2',
      ),
    ),
  ),
);
